
#include <iostream>
#include <vector>
using namespace std;

class Vector
{
private:
	double X;
	double Y;
	double Z;
public:
	Vector()
	{
	X = 0;
	Y = 0;
	Z = 0;
	}
	
	Vector(double x, double y, double z)
	{
		X = x;
		Y = y;
		Z = z;
	}
	double GetLength()
	{
		return sqrt(X * X + Y * Y + Z * Z);
	}
	void Print()
	{
		cout << endl;
		cout << "X=" << X;
		cout << endl;
		cout << "Y=" << Y;
		cout << endl;
		cout << "Z=" << Z;
		cout << endl;
	}
	Vector operator +(Vector r)
	{
		return Vector(X + r.X, Y + r.Y, Z + r.Z);
	}

	Vector operator -(Vector r)
	{
		return Vector(X - r.X, Y - r.Y, Z - r.Z);
	}

	double operator *(Vector r)
	{
		return (X * r.X + Y * r.Y + Z * r.Z);
	}
};
	
int main()
{
	Vector v(10, 10, 10);
	Vector v1(10, 10, 10);
	Vector v2;

	v2 = v+v2;
	v2.Print();

	v2 = v2-v1;
	v2.Print();

	double mul;
	mul = v*v1;
	cout << endl;
	cout << mul;

}
